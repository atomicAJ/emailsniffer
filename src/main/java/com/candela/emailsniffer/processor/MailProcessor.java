package com.candela.emailsniffer.processor;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.activation.DataHandler;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.json.simple.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MailProcessor implements Processor {

	public static final Logger LOGGER = LoggerFactory.getLogger(MailProcessor.class);

	@Value(value = "${candela.email.location}")
	private String location;

	@Value(value = "${candela.email.fileName}")
	private String fileName;

	private static final String PATH = "location";
	private static final String EMPTYSPACE = "\\s";
	private static final String EMPTY = "";
	private static final String COLON = ":";
	private static final String SUBJECT = "Subject";

	@Autowired
	ProducerTemplate producerTemplate;

	public void process(Exchange exchange) throws Exception {
		try {
			LOGGER.info("Processing the recieved email with subject: {} ", exchange.getIn().getHeader(SUBJECT));

			// In Case if you want to Create folder with time stamp
			/*
			 * Calendar cal = Calendar.getInstance(); Date date = cal.getTime(); String
			 * folderName = date.toString().replaceAll(EMPTYSPACE, EMPTY).replaceAll(COLON,
			 * EMPTY);
			 */

			// Creating custom Folders based on time and Email file on subject under it

			String folderName = exchange.getIn().getMessageId();
			folderName = location + folderName;
			LOGGER.debug("New folder will be created in the path : {}  and folder name is : {}", location, exchange.getIn().getMessageId());
			File stockDir = new File(folderName);
			stockDir.mkdir();
			javax.mail.Message mailMessage = exchange.getIn(org.apache.camel.component.mail.MailMessage.class)
					.getMessage();
			File efile = new File(stockDir, fileName);
			DataOutputStream os = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(efile)));
			mailMessage.writeTo(os);
			os.close();
			LOGGER.debug("Email is saved in the created folder succesfully");

			// Saving Attachments in the same folder
			Map<String, DataHandler> attachments = exchange.getIn().getAttachments();
			LOGGER.debug("Saving {} attachements in mail to same folder ", attachments.size());
			if (attachments.size() > 0) {
				for (String name : attachments.keySet()) {
					DataHandler dh = attachments.get(name);
					String attchementName = dh.getName();
					File file = new File(stockDir, attchementName);
					byte[] data = exchange.getContext().getTypeConverter().convertTo(byte[].class, dh.getInputStream());
					FileOutputStream out = new FileOutputStream(file);
					out.write(data);
					out.flush();
					out.close();
					LOGGER.debug("{} saved to the folder", attchementName);
				}
			}

			LOGGER.info("Processed the mail successfully, path of the processed mail will be pushed to Kafka");
			// Passing the file location to the Kafka
			JsonObject data = new JsonObject();
			data.put(PATH, folderName);
			producerTemplate.sendBody("direct:kafka", data.toString());

		} catch (Exception e) {
			LOGGER.error("Error while processing the email {}", e);
		}
	}
}
