package com.candela.emailsniffer.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.candela.emailsniffer.processor.MailProcessor;

public class CamelRoutes extends RouteBuilder {
	
	@Autowired
	MailProcessor mail;
	
	@Value(value = "${candela.email.server}")
	private String server;
	
	@Value(value = "${candela.email.username}")
	private String username;
	
	@Value(value = "${candela.email.password}")
	private String password;
	
	@Value(value = "${candela.email.readFolder}")
	private String readFolder;

	public void configure() {

		from(server+"?username="+username+"&password="+password
				+ "&copyTo="+readFolder+"&delete=true&unseen=true&consumer.delay=60000").process(mail)
		.to("kafka:emailSniffer?brokers=localhost:9092");
	}

}
