package com.candela.emailsniffer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:camel-route.xml")
public class EmailSnifferApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmailSnifferApplication.class, args);
	}

}
